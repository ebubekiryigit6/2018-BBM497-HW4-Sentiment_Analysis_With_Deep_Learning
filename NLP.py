import re
import string
import numpy as np
import tensorflow as tf

import constants
import utils

DATA_TYPE_POSITIVE = [1, 0]
DATA_TYPE_NEGATIVE = [0, 1]

LEARNING_RATE = 0.01
NUM_OF_EPOCHS = 41  # kere masallah for best accuracy :)
HIDDEN_LAYER_1 = 100
HIDDEN_LAYER_2 = 100
NUM_OF_CLASSES = 2


class TensorTrainer:
    def __init__(self, data_vectors, data_type_vectors, train_percent):
        self.__check_inputs(data_vectors, data_type_vectors)
        self.data_vectors = data_vectors
        self.data_type_vectors = data_type_vectors
        self.total_accuracy = 0.0
        self.train_percent = train_percent
        self.input_num = len(data_vectors[0])
        self.__prepareTraining()

    def __prepareTraining(self):
        utils.writeLog("TENSOR-TRAINER", "The vector data is being prepared for the train and test process.")
        X = tf.placeholder("float", [None, self.input_num])
        Y = tf.placeholder("float", [None, NUM_OF_CLASSES])
        weights = {
            constants.FIRST_HIDDEN_LAYER: tf.Variable(tf.random_normal([self.input_num, HIDDEN_LAYER_1])),
            constants.SECOND_HIDDEN_LAYER: tf.Variable(tf.random_normal([HIDDEN_LAYER_1, HIDDEN_LAYER_2])),
            constants.OUT_LAYER: tf.Variable(tf.random_normal([HIDDEN_LAYER_2, NUM_OF_CLASSES]))
        }
        biases = {
            constants.FIRST_BIAS: tf.Variable(tf.random_normal([HIDDEN_LAYER_1])),
            constants.SECOND_BIAS: tf.Variable(tf.random_normal([HIDDEN_LAYER_2])),
            constants.OUT_BIAS: tf.Variable(tf.random_normal([NUM_OF_CLASSES]))
        }
        self.__create_model(X, Y, weights, biases)

    def __check_inputs(self, data_vectors, data_type_vectors):
        if len(data_vectors) <= 0 or len(data_type_vectors) <= 0:
            raise ValueError("Data or type vectors cannot empty.")

    def __create_model(self, X, Y, weights, biases):
        # create hidden layers
        first_layer = tf.add(tf.matmul(X, weights[constants.FIRST_HIDDEN_LAYER]),
                             biases[constants.FIRST_BIAS])
        # added ReLu activation function
        first_layer = tf.nn.relu(first_layer)
        second_layer = tf.add(tf.matmul(first_layer, weights[constants.SECOND_HIDDEN_LAYER]),
                              biases[constants.SECOND_BIAS])
        second_layer = tf.nn.relu(second_layer)
        out_layer = tf.matmul(second_layer, weights[constants.OUT_LAYER]) + biases[constants.OUT_BIAS]

        loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(
            logits=out_layer, labels=Y))
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=LEARNING_RATE)
        train_op = optimizer.minimize(loss_op)
        correct_pred = tf.equal(tf.argmax(out_layer, 1), tf.argmax(Y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
        # default initializer
        init = tf.global_variables_initializer()

        utils.writeLog("TRAIN-MODEL", "Train model is created successfully.")

        self.train_op = train_op
        self.accuracy = accuracy
        self.init = init
        self.X = X
        self.Y = Y
        self.loss_op = loss_op

    def start_training(self):
        utils.writeLog("TENSOR-TRAINER", "Train process is started with"
                       + "\nNumber of Epochs: " + str(NUM_OF_EPOCHS)
                       + "\nLearning rate:    " + str(LEARNING_RATE)
                       + "\nHidden Layers:    " + "2"
                       + "\nTrain Percentage  " + str(self.train_percent) + "%")
        train_data, train_data_type = self.__get_train_vectors()
        test_data, test_data_type = self.__get_test_vectors()

        with tf.Session() as sess:
            sess.run(self.init)

            # run for each epoch
            for step in range(1, NUM_OF_EPOCHS + 1):
                # backprop
                sess.run(self.train_op, feed_dict={self.X: train_data, self.Y: train_data_type})
                sess.run([self.loss_op, self.accuracy], feed_dict={self.X: train_data,
                                                                   self.Y: train_data_type})

            self.total_accuracy = sess.run(self.accuracy, feed_dict={self.X: test_data,
                                                                     self.Y: test_data_type})
        utils.writeLog("TENSOR-TRAINER", "Train process is completed successfully.")
        return self

    def get_total_accuracy(self):
        return self.total_accuracy

    def __get_train_size(self):
        return round((len(self.data_vectors) / 100) * self.train_percent)

    def __get_train_vectors(self):
        train_size = self.__get_train_size()
        return self.data_vectors[:train_size], self.data_type_vectors[:train_size]

    def __get_test_vectors(self):
        train_size = self.__get_train_size()
        return self.data_vectors[train_size:], self.data_type_vectors[train_size:]


class SentimentParser:
    word_vectors = None
    data_file_path = None
    data_type = None

    sentence_vector = []

    def __init__(self, word_vectors):
        self.word_vectors = word_vectors
        self.sentence_vector = []

    def set_data(self, data_file_path):
        self.data_file_path = data_file_path
        return self

    def set_data_type(self, data_type):
        self.data_type = data_type
        return self

    def get_data_type(self):
        return self.data_type

    def get_sentence_vector(self):
        return self.sentence_vector

    def get_type_vector(self):
        # this is ridiculous i know but i cant find any other way :)
        if self.get_data_type() == DATA_TYPE_POSITIVE:
            return [DATA_TYPE_POSITIVE for i in range(len(self.sentence_vector))]
        elif self.get_data_type() == DATA_TYPE_NEGATIVE:
            return [DATA_TYPE_NEGATIVE for i in range(len(self.sentence_vector))]
        else:
            return []

    def execute(self):
        with open(self.data_file_path, "r", encoding=constants.DEFAULT_FILE_ENCODING) as file:
            for line in file:
                line = line.strip().strip("\n").strip()
                if line:
                    # replace punctuations with space
                    translated = line.maketrans(string.punctuation, ' ' * len(string.punctuation))
                    line = line.translate(translated).strip()
                    word_list = re.split(constants.ANY_WHITESPACE_REGEX, line)

                    # add vector arrays
                    sentence_vector = []
                    for word in word_list:
                        word = word.strip()
                        if word in self.word_vectors:
                            # first add
                            if len(sentence_vector) == 0:
                                sentence_vector = self.word_vectors[word]
                            # not first adds
                            else:
                                sentence_vector = np.add(sentence_vector, self.word_vectors[word])
                    self.sentence_vector.append(sentence_vector)
        file.close()
        return self
