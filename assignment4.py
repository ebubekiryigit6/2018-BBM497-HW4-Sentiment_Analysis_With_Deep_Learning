import re
import time
import sys

import numpy as np

import constants
import utils
import NLP

vectors_from_file = {}  # parsed vector dictionary


def populate_vectors(vector_file_path):
    """
    reads from vector file and parses words and vector floating points

    :param vector_file_path: file path to read vectors
    """
    with open(vector_file_path, "r", encoding=constants.DEFAULT_FILE_ENCODING) as file:
        for line in file:
            line = line.strip().strip("\n").strip()
            if line:
                key_value = line.split(":")
                if len(key_value) == 2:
                    key = key_value[0].strip()
                    temp_value = re.split(constants.ANY_WHITESPACE_REGEX, key_value[1].strip())
                    value = [float(x) for x in temp_value]  # cast to float
                    vectors_from_file[key] = value
    file.close()
    utils.writeLog("VECTOR-LOGGER", "Vector file parsed and saved in word:vector dictionary.")


def get_analysers(argv):
    """
    parses sentiment files (for this case positive and negative)

    :param argv: file arguments
    :return: positive analyser and negative analyser
    """
    positive_analyser = NLP.SentimentParser(vectors_from_file) \
        .set_data(argv[1]) \
        .set_data_type(NLP.DATA_TYPE_POSITIVE) \
        .execute()

    negative_analyser = NLP.SentimentParser(vectors_from_file) \
        .set_data(argv[2]) \
        .set_data_type(NLP.DATA_TYPE_NEGATIVE) \
        .execute()

    utils.writeLog("ANALYSER", "Positive and Negative analysers are created successfully.")
    return positive_analyser, negative_analyser


def get_vectors(pos_analyser, neg_analyser):
    """
    gets positive and negative vectors and combines each other
    :param pos_analyser: SentimentParser for positives
    :param neg_analyser: SentimentParser for negatives
    :return: all of the vectors and types
    """
    positive_vector = pos_analyser.get_sentence_vector()
    positive_type_vector = pos_analyser.get_type_vector()

    negative_vector = neg_analyser.get_sentence_vector()
    negative_type_vector = neg_analyser.get_type_vector()

    all_vector = np.concatenate([positive_vector, negative_vector])
    all_type_vector = np.concatenate([positive_type_vector, negative_type_vector])

    return all_vector, all_type_vector


def main(argv):
    populate_vectors(argv[3])

    positive_analyser, negative_analyser = get_analysers(argv)
    all_vector, all_type_vector = get_vectors(positive_analyser, negative_analyser)

    # get sequenced array like vector array
    randomize = np.arange(len(all_vector))
    # shuffle it
    np.random.shuffle(randomize)
    # random vector but same index with type vector
    all_vector = all_vector[randomize]
    all_type_vector = all_type_vector[randomize]

    utils.writeLog("SENTENCE-VECTOR", "All vectors and type vectors created successfully.")
    utils.writeLog("SYSTEM", "Train and test procedures will start in a second.")

    tensor_trainer = NLP.TensorTrainer(all_vector, all_type_vector, float(argv[4])) \
        .start_training()

    accuracy = tensor_trainer.get_total_accuracy()
    utils.writeLog("TENSOR-TRAINER", "Total Test Accuracy: " + str(accuracy))


if __name__ == "__main__":
    utils.writeLog("MAIN-PROCESS", "Sentiment Analysis With Deep Learning program is started.")
    start = time.time()
    main(sys.argv)
    utils.writeLog("MAIN-PROCESS",
                   "Sentiment Analysis With Deep Learning program is finished in "
                   + str(time.time() - start) + " seconds.")
