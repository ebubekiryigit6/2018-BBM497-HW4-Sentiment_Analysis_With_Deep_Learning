DEFAULT_FILE_ENCODING = 'UTF-8'  # for Turkish chars

ANY_WHITESPACE_REGEX = r'\s+'

FIRST_HIDDEN_LAYER = "hidden_layer_1"
SECOND_HIDDEN_LAYER = "hidden_layer_2"
OUT_LAYER = "out_layer"

FIRST_BIAS = "bias_1"
SECOND_BIAS = "bias_2"
OUT_BIAS = "out_bias"

DEFAULT_TRAIN_PERCENTENCE = 75

LOG_FILE = "console.log"
