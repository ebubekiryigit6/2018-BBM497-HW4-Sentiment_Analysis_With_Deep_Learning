#  Sentiment Analysis With Deep Learning  

BBM497 - Introduction to NLP Lab.

Assignment 4

### Author
Ebubekir Yiğit - 21328629



### Project Introduction
In the project we will learn positive and negative sentences 
with a neural network, test other sentences and calculate accuracies. 
To do this, we will first parse the pre-trained vectors and 
translate the vectors into vectors. 

We will parse the words in the vector file like:
```text
duymak:0.00039987528 0.1955209 -0.059512615 -0.113120824 -0.02492363
kendini:-0.19460249 0.08723577 -0.01318719 -0.2033351 0.08306696
Istanbul:0.22515526 -0.60302854 0.05672372 0.51640266 -0.0031004623
İstanbul:-0.03973162 0.053483784 0.056805722 0.2733361 -0.106442355
kadar:0.12575074 -0.32005242 -0.09062646 -0.2756619 0.036881454

```

## Getting Started

In the project we first parse the vector file and create a "word: vector" dictionary. 
We then read the files of positive and negative cues. 
For each word in the sentences in it 
we have collected the vectors found in the vector dictionary. 
However, there were a number of positive and negative sentence vectors left. 
We shuffled along with this type of knee to get new data. 
We distinguished this data by the percentage given as train and test. 
We have trained this data in 2 layers with the TensorFlow library.
While we were training the data, we chose the Gradient Descent optimizer as the optimizer. 
At the same time we added the ReLu activation function. 
We run and train with the number of epochs you have set. 
We then obtained the test accuracy by running the test.

**All operations are progressively saved to the console.log file. 
You can browse the console.log file to see the operations.**


**SentimentParser:**
```python
    import NLP 
    
    positive_analyser = NLP.SentimentParser(word_vector_dictionary) \
            .set_data(positive_data_path) \
            .set_data_type(NLP.DATA_TYPE_POSITIVE) \
            .execute()
    
    negative_analyser = NLP.SentimentParser(word_vector_dictionary) \
            .set_data(negative_data_path) \
            .set_data_type(NLP.DATA_TYPE_NEGATIVE) \
            .execute()
```


**TensorTrainer:**
```python
    import NLP
    
    tensor_trainer = NLP.TensorTrainer(all_vector, all_type_vector, train_percent) \
            .start_training()

    accuracy = tensor_trainer.get_total_accuracy()
```   


### Output
**There is no output in standard output. Only exist in console.log file.**

**I got a maximum of 89% accuracy.**


### Prerequisites

Python 3.5 is required to run the project. 

**Used Python libraries:**

- TensorFlow - NumPy


For Ubuntu:

```bash
sudo apt-get install python3.5
pip3 install tensorflow
```

For Centos:
```bash
sudo yum -y install python35u
pip3 install tensorflow
```

### Running the Project

python3 assignment4.py <positive_file_path> <negative_file_path> <vector_file_path> <train_percent>

Example:
```text
python3 assignment4.py positives.txt negatives.txt vectors.txt 75
```

**I got a maximum of 89% accuracy.**

**After run, please see "console.log" file to show process logs.**

### console.log File
```text
23:49:02   LOG/ MAIN-PROCESS:  Sentiment Analysis With Deep Learning program is started.
23:49:02   LOG/ VECTOR-LOGGER:  Vector file parsed and saved in word:vector dictionary.
23:49:02   LOG/ ANALYSER:  Positive and Negative analysers are created successfully.
23:49:02   LOG/ SENTENCE-VECTOR:  All vectors and type vectors created successfully.
23:49:02   LOG/ SYSTEM:  Train and test procedures will start in a second.
23:49:02   LOG/ TENSOR-TRAINER:  The vector data is being prepared for the train and test process.
23:49:02   LOG/ TRAIN-MODEL:  Train model is created successfully.
23:49:02   LOG/ TENSOR-TRAINER:  Train process is started with
                                 Number of Epochs: 41
                                 Learning rate:    0.01
                                 Hidden Layers:    2
                                 Train Percentage  75.0%
23:49:03   LOG/ TENSOR-TRAINER:  Train process is completed successfully.
23:49:03   LOG/ TENSOR-TRAINER:  Total Test Accuracy: 0.8378378
23:49:03   LOG/ MAIN-PROCESS:  Sentiment Analysis With Deep Learning program is finished in 0.4940071105957031 seconds.
```

**I got a maximum of 89% accuracy.**

### References

Lecture Slides

https://stackoverflow.com

https://github.com/aymericdamien/TensorFlow-Examples