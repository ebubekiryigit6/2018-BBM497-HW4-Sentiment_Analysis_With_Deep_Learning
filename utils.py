import pickle
import time

import constants


def writeLineToFile(file, buffer):
    """
    writes a buffer line to file object

    :param file: opened file object
    :param buffer: buffer to write line
    """
    toWrite = buffer + "\n"
    file.write(toWrite)


def writeLog(tag, log):
    """
    Writes logs to console log file.

    :param tag: log tag
    :param log: log message
    """
    f = open(constants.LOG_FILE, "a")
    start = time.strftime("%H:%M:%S") + "   LOG/ " + tag.upper() + ":  "
    log = log.replace("\n", "\n" + (" " * len(start)))
    buffer = start + log + "\n"
    f.write(buffer)
    f.close()


def writeAsSerializable(array, fileName):
    """
    Writes array to file as serializable.

    :param array: array to dump
    :param fileName: which file?
    """
    file = open(fileName, 'wb')
    pickle.dump(array, file)
    file.close()
    writeLog("PICKLE", "PICKLE file is created as " + fileName)


def getObjectFromPickle(fileName):
    """
    Gets array from pickle file.

    :param fileName: file that contains binary values of array
    :return: an array from given file
    """
    file = open(fileName, 'rb')
    array = pickle.load(file)
    # file.close()
    writeLog("PICKLE", "Array is get from pickle file " + fileName)
    return array
